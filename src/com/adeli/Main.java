package com.adeli;

import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {

        ExecutorService tpool = Executors.newFixedThreadPool(100);
        Thread t = new Thread(Server.getInstance());
        t.start();

        for (int i = 0; i < 1000; i++)
            tpool.execute(new MyRunnable(i));

        tpool.shutdown();
    }
}
