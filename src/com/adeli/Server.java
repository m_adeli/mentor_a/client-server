package com.adeli;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Server implements Runnable{
    private static Server srv;
    private BlockingQueue<Client> clients = new LinkedBlockingQueue<>(20);

    private Server() {
    }

    public static Server getInstance() {
        if (srv == null)
            srv = new Server();
        return srv;
    }

    public void addToQueue(Client client) {
        try {
            clients.put(client);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void serve(String str) {
        try {
            Thread.sleep(10);
            System.out.println(str);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                serve(clients.take().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
