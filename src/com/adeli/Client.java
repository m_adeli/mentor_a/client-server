package com.adeli;

public class Client {
    private String name;

    public Client(int name) {
        this.name = String.valueOf(name);
    }

    public String getName() {
        return name;
    }
}
